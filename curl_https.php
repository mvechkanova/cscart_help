<?php
	  error_reporting(E_ALL ^ E_NOTICE);
	  ini_set("display_errors", "on");	  
	  
          $curlDefault = array(
	  CURLOPT_PORT => 443, //ignore explicit setting of port 80
	  CURLOPT_RETURNTRANSFER => TRUE,
	  //CURLOPT_FOLLOWLOCATION => TRUE,
	  CURLOPT_ENCODING => '',
	  CURLOPT_HTTPHEADER => array(
	      'Proxy-Connection: Close',
	      'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1017.2 Safari/535.19',
	      'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
	      'Accept-Encoding: gzip,deflate,sdch',
	      'Accept-Language: en-US,en;q=0.8',
	      'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3',
	     // 'Cookie: __qca=blabla',
	      'Connection: Close',
	  ),
	  CURLOPT_VERBOSE => TRUE, // TRUE to output verbose information. Writes output to STDERR, or the file specified using CURLOPT_STDERR.
	  CURLOPT_STDERR => $verbose = fopen('curl_debug.txt', 'w+')
      );

      $url = "https://www.activitiesrightnow.com/ARNStore/index.php?dispatch=index.index&check_https=Y";
      $handle = curl_init($url);
      curl_setopt_array($handle, $curlDefault);
      $html = curl_exec($handle);
      //$urlEndpoint = curl_getinfo($handle, CURLINFO_EFFECTIVE_URL);
      //echo "Verbose information:\n<pre>", !rewind($verbose), htmlspecialchars(stream_get_contents($verbose)), "</pre>\n";
      curl_close($handle);
      ?>
