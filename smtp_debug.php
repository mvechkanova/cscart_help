<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

include ('class.phpmailer.php');
$mail = new PHPMailer();
$mail->IsSMTP();
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'SMTP_SECURE_TYPE';
$mail->Host = 'SMTP_SERVER_HOSTNAME';
$mail->Port = SMTP_SERVER_PORT;
$mail->SMTPDebug = 2;
$mail->Username = 'SMTP_USERNAME';
$mail->Password = 'SMTP_PASSWORD';
$mail->SetFrom('CUSTOMER_EMAIL_ADDRESS');
$mail->FromName = 'From';
$mail->AddAddress('info@sandspointshop.com');
$mail->Subject = 'Test message';
$mail->Body = '<H3>This is a test message!</H3>';
$mail->IsHTML (true);

if (!$mail->Send()) {
echo "Error: $mail->ErrorInfo";
} else {
echo 'Message Sent!';
}