<?php
//for old versions
// define('AREA', 'C');
// define('AREA_NAME', 'customer');
// define('ACCOUNT_TYPE', 'customer');
// define('DEVELOPMENT', true);
//
// require dirname(__FILE__) . '/prepare.php';
// require dirname(__FILE__) . '/init.php';
//
// define('INDEX_SCRIPT', Registry::get('config.customer_index'));


//for 4.x
require dirname(__FILE__) . '/init.php';
require dirname(__FILE__) . '/config.local.php';
require dirname(__FILE__) . '/config.php';
define('DEVELOPMENT', true);
error_reporting(E_ALL ^ E_NOTICE);
ini_set("display_errors", "on");

$tables_info = array(
	array(
		'table' => 'product_options_inventory',
		'field' => 'combination_hash',
		'primary' => array('combination_hash'),
	),
);

foreach ($tables_info as $t_info) {
	db_query("ALTER TABLE ?:" . $t_info['table'] . " ADD `new_" . $t_info['field'] . "` INT( 11 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `" . $t_info['field'] . "`");
	$i = 0;
	$step = 50;
	$fields = $t_info['primary'];
	$fields[] = $t_info['field'];
	$fields = array_unique($fields);
	while ($data = db_get_array("SELECT " . implode(', ', $fields) . " FROM ?:" . $t_info['table'] . " LIMIT $i, $step")) {
        $i += $step;
		foreach ($data as $row) {
			$id = (float) $row[$t_info['field']];
			fn_print_r("id:".$id);
			if ($id >= 0x80000000) {
				$new_value = $id / 2;
				$new_value = (int) $new_value;
			} else {
				$new_value = $id;
			}
			fn_print_r("new value:".$new_value);
			db_query("UPDATE ?:" . $t_info['table'] . " SET `new_" . $t_info['field'] . "` = ?i WHERE ?w", $new_value, $row);
		}
	}
	//fn_print_die("stop");
	if (in_array($t_info['field'], $t_info['primary'])) {
		db_query("ALTER TABLE ?:" . $t_info['table'] . " DROP PRIMARY KEY");
	}
	db_query("ALTER TABLE ?:" . $t_info['table'] . " DROP `" . $t_info['field'] . "`");
	db_query("ALTER TABLE ?:" . $t_info['table'] . " CHANGE `new_" . $t_info['field'] . "` `" . $t_info['field'] . "` INT( 11 ) UNSIGNED NOT NULL DEFAULT '0'");
	if (in_array($t_info['field'], $t_info['primary'])) {
		db_query("ALTER TABLE ?:" . $t_info['table'] . " ADD PRIMARY KEY ( " . implode(', ', $t_info['primary']) . " )");
	}
}
echo "done";
