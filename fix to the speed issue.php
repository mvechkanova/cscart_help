Here are the instructions:

1. Open the <b>core/classes/bm/render_manager.php</b> file.
2. Find the following part of code:

<code>
         'top' => $this->_render_container($this->containers['TOP']),
         'central' => $this->_render_container($this->containers['CENTRAL']),
         'bottom' => $this->_render_container($this->containers['BOTTOM']),
      ));
</code>

3. Replace the following line:

<code>
));
</code>

with this one:

<code>
), false);
</code>

4. Find the following part of code in the same file:

<code>
}
     }

    $this->view->assign('content', $content);
</code>

5. Replace the following line:

<code>
$this->view->assign('content', $content);
</code>

with this one:

<code>
$this->view->assign('content', $content, false);
</code>

6. Find the following part of code in the same file:

<code>
$content .= $this->render_blocks($grid);
     }

    $this->view->assign('content', $content);
</code>

7. Replace the following line:

<code>
$this->view->assign('content', $content);
</code>

with this one:

<code>
$this->view->assign('content', $content, false);
</code>

8. Replace the following line:

<code>
Registry::get_view()->assign('content', $block_content);
</code>

with this one:

<code>
Registry::get_view()->assign('content', $block_content, false);
</code>

(Do it in both cases)

9. Add the following lines:

<code>
$cookie_data = fn_get_session_data();
$cookie_data['all'] = $cookie_data;
</code>

after these ones:

<code>
        $handlers = array();
       }
</code>

10. In the same file, add the following line:

<code>
$additional_level .= self::_generate_additional_cache_level($block_scheme['cache'], 'cookie_handlers', $cookie_data);
</code>

after this one:

<code>
$additional_level .= self::_generate_additional_cache_level($block_scheme['cache'], 'session_handlers', $_SESSION);
</code>

11. Open the <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/views/block_manager/render/grid.tpl</b> file where <strong>[CUSTOMER_ACTIVE_SKIN]</strong> is an active skin of your storefront.

12. Replace the following line:

<code>
{$content|unescape}
</code>

with this one:

<code>
{$content}
</code>

13. Open the <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/views/block_manager/render/location.tpl</b> file.
14. Replace the following lines:

<code>
<div id="ci_top_wrapper" class="header clearfix">
{$containers.top|htmlspecialchars_decode|unescape}
 <!--ci_top_wrapper--></div>
 <div id="ci_central_wrapper" class="main clearfix">
{$containers.central|htmlspecialchars_decode|unescape}
 <!--ci_central_wrapper--></div>
 <div id="ci_bottom_wrapper" class="footer clearfix">
{$containers.bottom|htmlspecialchars_decode|unescape}
 <!--ci_bottom_wrapper--></div>
</code>

with these ones:

<code>
<div id="ci_top_wrapper" class="header clearfix">
{$containers.top}
<!--ci_top_wrapper--></div>
<div id="ci_central_wrapper" class="main clearfix">
{$containers.central}
 <!--ci_central_wrapper--></div>
 <div id="ci_bottom_wrapper" class="footer clearfix">
{$containers.bottom}
 <!--ci_bottom_wrapper--></div>
</code>

15. Open the <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/blocks/wrappers/mainbox_general.tpl</b> file.
16. Replace the following line:

<code>
<div class="mainbox-body">{$content|unescape}</div>
</code>

with this one:

<code>
<div class="mainbox-body">{$content}</div>
</code>

17. Open the <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/blocks/wrappers/mainbox_simple.tpl</b> file.
18. Replace the following line:

<code>
<div class="mainbox2-body">{$content|unescape}</div>
</code>

with this one:

<code>
<div class="mainbox2-body">{$content}</div>
</code>

19. Open the <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/blocks/wrappers/onclick_dropdown.tpl</b> file.
20. Replace the following line:

<code>
{$content|unescape|default:"&nbsp;"}
</code>

with this one:

<code>
{$content|default:"&nbsp;"}
</code>

21. Open the <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/blocks/wrappers/sidebox_general.tpl</b> file.
22. Replace the following line:

<code>
<div class="sidebox-body">{$content|unescape|default:"&nbsp;"}</div>
</code>

with this one:

<code>
<div class="sidebox-body">{$content|default:"&nbsp;"}</div>
</code>

23. Open the <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/blocks/wrappers/sidebox_important.tpl</b> file.
24. Replace the following line:

<code>
<div class="sidebox-body">{$content|unescape|default:"&nbsp;"}</div>
</code>

with this one:

<code>
<div class="sidebox-body">{$content|default:"&nbsp;"}</div>
</code>

25. Open the <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/views/block_manager/render/block.tpl</b> file.
26. Replace the following line:

<code>
{$content|unescape}
</code>

with this one:

<code>
{$content}
</code>

27. Open the <b>core/fn.common.php</b> file.
28. Add the following lines:

<code>
if (!empty($expiry)) {
    $_SESSION['settings'][$var]['expiry'] = TIME + $expiry;
  }
</code>

after these ones:

<code>
     $_SESSION['settings'][$var] = array (
     'value' => $value
   );
</code>

29. In the same file, replace the following line:

<code>
function fn_get_session_data($var)
</code>

with this one:

<code>
function fn_get_session_data($var = '')
</code>

30. In the same file, replace the following line:

<code>
return isset($_SESSION['settings'][$var]['value']) ? $_SESSION['settings'][$var]['value'] : '';
</code>

with these lines:

<code>
 if (!$var) {
    $return = array();
    foreach ($_SESSION['settings'] as $name => $setting) {
      if (empty($setting['expiry']) || $setting['expiry'] > TIME) {
        $return[$name] = $setting['value'];
      } else {
        unset($_SESSION['settings'][$name]);
      }
    }
  } else {
    if (!empty($_SESSION['settings'][$var]) && (empty($_SESSION['settings'][$var]['expiry']) ||  $_SESSION['settings'][$var]['expiry'] > TIME)) {
      $return = isset($_SESSION['settings'][$var]['value']) ? $_SESSION['settings'][$var]['value'] : '';
    } else {
      if (!empty($_SESSION['settings'][$var])) {
        unset($_SESSION['settings'][$var]);
      }

      $return = false;
    }
  }

  return $return;
</code>

31. Open the <b>schemas/block_manager/blocks.php</b> file.
32. Replace the following line:

<code>
'session_handlers' => array ('settings' => '%SETTINGS%')
</code>

with this one:

<code>
'cookie_handlers' => array ('%ALL%')
</code>

33. Clean up the template cache. In order to do it, please open this link in your browser: http://www.your_domain.com/admin.php?cc where &nbsp;<strong>www.your_domain.com</strong>&nbsp; is the address of your store, and where <strong>admin.php</strong> is a script file for the administration panel of your store, which was renamed for security reasons. Please note that before cleaning the template cache you should be logged in to the administration panel of your store.

Please check it and let me know the result.

Thank you.


************************

RUS

Здравствуйте, Егор.

Спасибо за ожидание. Приносим извинения за задержку с ответом.

Для улучшения скорости загрузки страниц в магазине, пожалуйста, сделайте следующие изменения:

1. Откройте файл <b>core/classes/bm/render_manager.php</b>.
2. Найдите следующую часть кода:
<code>         'top' => $this->_render_container($this->containers['TOP']),
         'central' => $this->_render_container($this->containers['CENTRAL']),
         'bottom' => $this->_render_container($this->containers['BOTTOM']),
      ));</code>

3. Замените эту часть кода:
<code>));</code>

на эту:
<code>), false);</code>

4. Найдите следующую часть кода в этом же файле:
<code>}
     }

    $this->view->assign('content', $content);
</code>

5. Замените эту часть кода:
<code>$this->view->assign('content', $content);</code>

на эту:
<code>$this->view->assign('content', $content, false);</code>

6. Найдите следующую часть кода в этом же файле:
<code>$content .= $this->render_blocks($grid);
     }

    $this->view->assign('content', $content);
</code>

7. Замените эту часть кода:
<code>$this->view->assign('content', $content);</code>

на эту:
<code>$this->view->assign('content', $content, false);
</code>

8. Добавьте следующую часть кода:
<code>$cookie_data = fn_get_session_data();
$cookie_data['all'] = $cookie_data;
</code>

после этой части кода:
<code>        $handlers = array();
    }
</code>

9. Добавьте следующую часть кода:
<code>$additional_level .= self::_generate_additional_cache_level($block_scheme['cache'], 'cookie_handlers', $cookie_data);
</code>

после этой:
<code>$additional_level .= self::_generate_additional_cache_level($block_scheme['cache'], 'session_handlers', $_SESSION);
</code>

10. Найдите и замените эту часть кода:
<code>Registry::get_view()->assign('content', $block_content);
</code>

на эту:
<code>Registry::get_view()->assign('content', $block_content, false);</code>

(Сделайте данные изменения 2 раза)

11. Откройте файл <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/views/block_manager/render/grid.tpl</b>, где <b>[CUSTOMER_ACTIVE_SKIN]</b> - активная тема (скин) вашего магазина.
12. Замените эту часть кода:
<code>{$content|unescape}</code>

на эту:
<code>{$content}</code>

13. Откройте файл <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/views/block_manager/render/location.tpl</b>.
14. Замените следующую часть кода:
<code><div id="ci_top_wrapper" class="header clearfix">
{$containers.top|htmlspecialchars_decode|unescape}
 <!--ci_top_wrapper--></div>
 <div id="ci_central_wrapper" class="main clearfix">
{$containers.central|htmlspecialchars_decode|unescape}
 <!--ci_central_wrapper--></div>
 <div id="ci_bottom_wrapper" class="footer clearfix">
{$containers.bottom|htmlspecialchars_decode|unescape}
 <!--ci_bottom_wrapper--></div>
</code>

на эту:
<code><div id="ci_top_wrapper" class="header clearfix">
{$containers.top}
<!--ci_top_wrapper--></div>
<div id="ci_central_wrapper" class="main clearfix">
{$containers.central}
 <!--ci_central_wrapper--></div>
 <div id="ci_bottom_wrapper" class="footer clearfix">
{$containers.bottom}
 <!--ci_bottom_wrapper--></div>
</code>

15. Откройте файл <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/blocks/wrappers/mainbox_general.tpl</b>.
16. Замените эту часть кода:
<code><div class="mainbox-body">{$content|unescape}</div></code>

на эту:
<code><div class="mainbox-body">{$content}</div></code>

17. Откройте файл <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/blocks/wrappers/mainbox_simple.tpl</b>.
18. Замените эту часть кода:
<code><div class="mainbox2-body">{$content|unescape}</div></code>

на эту:
<code><div class="mainbox2-body">{$content}</div></code>

19. Откройте файл <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/blocks/wrappers/onclick_dropdown.tpl</b>.
20. Замените эту часть кода:
<code>{$content|unescape|default:"&nbsp;"}</code>

на эту:
<code>{$content|default:"&nbsp;"}</code>

21. Откройте файл <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/blocks/wrappers/sidebox_general.tpl</b>.
22. Замените эту часть кода:
<code><div class="sidebox-body">{$content|unescape|default:"&nbsp;"}</div></code>

на эту:
<code><div class="sidebox-body">{$content|default:"&nbsp;"}</div></code>

23. Откройте файл <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/blocks/wrappers/sidebox_important.tpl</b>.
24. Замените эту часть кода:
<code><div class="sidebox-body">{$content|unescape|default:"&nbsp;"}</div></code>

на эту:
<code><div class="sidebox-body">{$content|default:"&nbsp;"}</div></code>

25. Откройте файл <b>skins/[CUSTOMER_ACTIVE_SKIN]/customer/views/block_manager/render/block.tpl</b>.
26. Замените эту часть кода:
<code>{$content|unescape}</code>

на эту:
<code>{$content}</code>

27. Откройте файл <b>core/fn.common.php</b>.
28. Добавьте эту часть кода:
<code>if (!empty($expiry)) {
    $_SESSION['settings'][$var]['expiry'] = TIME + $expiry;
  }
</code>

после этой части кода:
<code>     $_SESSION['settings'][$var] = array (
     'value' => $value
   );
</code>

29. Замените эту часть кода:
<code>function fn_get_session_data($var)</code>

на эту:
<code>function fn_get_session_data($var = '')</code>

30. Замените эту часть кода:
<code>return isset($_SESSION['settings'][$var]['value']) ? $_SESSION['settings'][$var]['value'] : '';</code>

на эту часть кода:
<code>if (!$var) {
    $return = array();
    foreach ($_SESSION['settings'] as $name => $setting) {
      if (empty($setting['expiry']) || $setting['expiry'] > TIME) {
        $return[$name] = $setting['value'];
      } else {
        unset($_SESSION['settings'][$name]);
      }
    }
  } else {
    if (!empty($_SESSION['settings'][$var]) && (empty($_SESSION['settings'][$var]['expiry']) ||  $_SESSION['settings'][$var]['expiry'] > TIME)) {
      $return = isset($_SESSION['settings'][$var]['value']) ? $_SESSION['settings'][$var]['value'] : '';
    } else {
      if (!empty($_SESSION['settings'][$var])) {
        unset($_SESSION['settings'][$var]);
      }

      $return = false;
    }
  }

  return $return;
</code>
31. Откройте файл <b>schemas/block_manager/blocks.php</b>.
32. Замените эту часть кода:
<code>'session_handlers' => array ('settings' => '%SETTINGS%')
</code>

на эту:
<code>'cookie_handlers' => array ('%ALL%')</code>

33. Почистите кеш шаблонов. Для того чтобы почистить кеш шаблонов вашего магазина, пожалуйста, авторизуйтесь в администраторской части вашего магазина и откройте следующую ссылку в браузере:
http://www.your_domain.com/admin.php?cc , где www.your_domain.com - доменное имя вашего сайта и где admin.php - имя скрипта администраторской части вашего магазина.

Данное решение должно улучшить быстродействие магазина. Пожалуйста, проверьте и сообщите нам о результате.

Если у вас возникнут трудности, пожалуйста, предоставьте нам временный доступ по FTP к вашему серверу, нажав на ссылку <b>Add record</b> на странице <b>Access information</b> вашей учетной записи Help Desk, и сообщите нам об этом в этом же тикете.

Спасибо.
