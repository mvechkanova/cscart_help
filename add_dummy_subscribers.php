<?php

function fn_update_subscriber($subscriber_data, $subscriber_id = 0)
{
    $invalid_emails = array();
    if (empty($subscriber_data['list_ids'])) {
        $subscriber_data['list_ids'] = array();
    }
    if (empty($subscriber_data['mailing_lists'])) {
        $subscriber_data['mailing_lists'] = array();
    }

    if (empty($subscriber_id)) {
        if (!empty($subscriber_data['email'])) {
            if (db_get_field("SELECT email FROM ?:subscribers WHERE email = ?s", $subscriber_data['email']) == '') {
                if (fn_validate_email($subscriber_data['email']) == false) {
                    $invalid_emails[] = $subscriber_data['email'];
                } else {
                    $subscriber_data['timestamp'] = TIME;
                    $subscriber_id = db_query("INSERT INTO ?:subscribers ?e", $subscriber_data);
                }
            } else {
                fn_set_notification('W', __('warning'), __('warning_subscr_email_exists', array(
                    '[email]' => $subscriber_data['email']
                )));
            }
        }
    } else {
        db_query("UPDATE ?:subscribers SET ?u WHERE subscriber_id = ?i", $subscriber_data, $subscriber_id);
    }

    fn_update_subscriptions($subscriber_id, $subscriber_data['list_ids'], isset($subscriber_data['confirmed']) ? $subscriber_data['confirmed'] : $subscriber_data['mailing_lists'], fn_get_notification_rules($subscriber_data), $subscriber_data['lang_code']);

    if (!empty($invalid_emails)) {
        fn_set_notification('E', __('error'), __('error_invalid_emails', array(
            '[emails]' => implode(', ', $invalid_emails)
        )));
    }

    return $subscriber_id;
}

require dirname(__FILE__) . '/init.php';
require dirname(__FILE__) . '/config.local.php';
require dirname(__FILE__) . '/config.php';
define('DEVELOPMENT', true);
error_reporting(E_ALL ^ E_NOTICE);
ini_set("display_errors", "on");


for ($i = 4; $i <= 6500; $i++) {
	$email = 'test'.$i.'@example.com';
	$subscriber_data = array('email' => $email, 'lang_code' => 'en');
	fn_update_subscriber($subscriber_data);
}
echo "done";
?>
