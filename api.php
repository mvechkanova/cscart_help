<?php

error_reporting(E_ALL ^ E_NOTICE);
ini_set("display_errors", "on");

// SET UP YOUR CREDENTIALS HERE
$credentials = "email:APIkey";
$body='{
  "order_id": "98",
  "is_parent_order": "N",
  "parent_order_id": "0",
  "company_id": "1",
  "issuer_id": null,
  "user_id": "1",
  "total": "74.17",
  "subtotal_discount": "0.00",
  "payment_surcharge": "0.00",
  "shipping_ids": "1",
  "shipping_cost": "65.00",
  "timestamp": "1436441088",
  "status": "D",
  "payment_id": "2",
  "shipping_ids": "1"
  }';
$ch = curl_init();
curl_setopt($ch, CURLOPT_USERPWD, $credentials);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  'Content-Type: application/json',
  'Authorization: Basic ' . base64_encode($credentials)
));
//open connection

//replace [ORDER_ID] with the desired ID
$url = "http://your_domain.com/api/orders/[ORDER_ID]";


// //
curl_setopt($ch, CURLOPT_URL, $url);
//set up method
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

$result = curl_exec($ch);
curl_close($ch);

//var_dump($result);
//print_r($result);

?>
