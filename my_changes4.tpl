The content of the <b>design/themes/basic/templates/addons/my_changes/hooks/index/styles.post.tpl</b> file should be the following:
<code>
{style src="addons/my_changes/styles.css"}
</code>
Please make sure your <b>styles.post.tpl</b> and <b>styles.css</b> files are located in correct directories.
The <b>styles.post.tpl</b> file should be located in the directory:
<code>
design/themes/basic/templates/addons/my_changes/hooks/index/
</code>
CSS files to be included from the hook should be in the directory:
<code>
design/themes/basic/css/addons/my_changes/
</code>
Also please note that in CS-Cart 4.0.1 all included css files are combined into one file. It is named like <b>standalone.[HASH].css</b>.
So you will not see separate files including the <b>my_changes/styles.css</b> file. But you should check the <b>standalone.[HASH].css</b> file for new styles. Also, you may have to clear your store cache and your browser cache to see changes in css.

Please check it.

Thank you.