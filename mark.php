#!/usr/bin/php
<?php

if (empty($argv[1])) {
	echo("Usage: put to CS-Cart root directory and run\n");
	die('./mark_package.php COMPANY_ID');
}

$company_id = $argv[1];

define('CDIR', dirname(__FILE__));

// List of files to be marked
$markers = '
<files>
	<file name="lib/templater/debug.tpl">
		<mark>{* debug.tpl, last updated version 2.1.0 *}</mark>
	</file>
	<file name="lib/phpmailer/changelog.txt">
		<mark>* Added Dutch, Swedish, Czech, Norwegian, and Turkish translations.</mark>
	</file>
</files>
';

$xml = simplexml_load_string($markers);
foreach ($xml as $item) {
	$re[(string)$item['name']] = array();
	if (!empty($item->mark)) {
		foreach ($item->mark as $m) {
			$markers_data[(string)$item['name']][(string)$m] = true;
		}
	}
}


// Prepare markers
$i = 0;
$needles = array();
$replacements = array();
$marker_files = array();
foreach ($markers_data as $file => $data) {
	$marker_files[] = $file;
	foreach ($data as $k => $v) {
		$i++;
		$needles[$i] = trim($k);
		$replacements[$i] = substr_replace($needles[$i], $company_id, 2, strlen($company_id));
	}
}

foreach ($marker_files as $m) {
	$s = file_get_contents(CDIR . '/' . $m);
	$s = str_replace($needles, $replacements, $s);
	
	file_put_contents(CDIR . '/' . $m, $s);
}

echo "Done: marked with $company_id\n";


?>
