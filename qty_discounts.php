<?php

//remove undeleted qty discounts due to bug 1-14508 
// 4.x.x
define('AREA', 'A');
define('ACCOUNT_TYPE', 'admin');
require(dirname(__FILE__) . '/init.php');

$prices = db_get_array("SELECT product_prices.product_id, product_prices.lower_limit, usergroup_id, product_prices.percentage_discount, IF(product_prices.percentage_discount = 0, product_prices.price, prices.price - (product_prices.price * prices.percentage_discount)/100) as price FROM $table_name product_prices ORDER BY usergroup_id, lower_limit");

foreach ($prices as $price) {
    fn_print_r($price);
}

echo 'Done';