<?php
// 4.x.x
define('AREA', 'A');
define('ACCOUNT_TYPE', 'admin');
require(dirname(__FILE__) . '/init.php');

// 3.x.x
// DEFINE ('AREA', 'A');
// DEFINE ('AREA_NAME' ,'admin');
// DEFINE ('ACCOUNT_TYPE', 'admin');
// require(dirname(__FILE__) . '/prepare.php');
// require(dirname(__FILE__) . '/init.php');

$users = db_get_array("SELECT user_id, user_login FROM ?:users WHERE user_type = 'C' AND user_login LIKE '%user%'");

foreach ($users as $user) {
    db_query("UPDATE ?:users SET user_login = 'user_?i' WHERE user_id = ?i", $user['user_id'], $user['user_id']);
}

echo 'Done';