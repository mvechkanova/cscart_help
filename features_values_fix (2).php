<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

define('AREA', 'C');

require dirname(__FILE__) . '/prepare.php';
require dirname(__FILE__) . '/init.php';

define('INDEX_SCRIPT',  Registry::get('config.customer_index'));

$features_values = db_get_array("SELECT * FROM ?:product_features_values");
foreach ($features_values as $feature_value) {
    $variant_ids = db_get_fields("SELECT variant_id FROM ?:product_features_values WHERE feature_id = ?i AND product_id = ?i AND lang_code = ?s", $feature_value['feature_id'], $feature_value['product_id'], $feature_value['lang_code']);
    foreach ($variant_ids as $variant_id) {
        if ($variant_id != $feature_value['variant_id']) {
            db_query("DELETE FROM ?:product_features_values WHERE feature_id = ?i AND product_id = ?i AND variant_id = ?i AND lang_code = ?s", $feature_value['feature_id'], $feature_value['product_id'], $feature_value['variant_id'], $feature_value['lang_code']);
        }
    }
}

die('sucess');

?>
